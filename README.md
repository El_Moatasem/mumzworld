# Mumz World 

I applied **VIPER** Architecture with replacement of Router by **Coordinator** as it has the flavor or commnunication with another **Coordinator**  
and **Dependency Injection** to inject the objects that are dependant on each other 

The Architecture follows **S.O.L.I.D** principles which are  :

* Single Responsibility Principle :   every module do only 1 job
* Open/Closed Principle : modules open for extension and closed for modification 
* Liskov Substitution Principle : All Types can be replaced by subtypes with no changes
* Interface Segregation Principle : No dependency on unused methods  
* Dependency Inversion : Details depend on abstraction and abstraction doesn't depend on details 


## Getting Started

Each Module has the following Components :

* Coordinator
* Builder
* Presenter
* Interactor
* View
 

We have the following Interfaces :

* Coordinator Interface
* Coordinator Builder Interface
* View Builder Interface
* View Interface
* Data Manager Interface
* Persistent Store Data Manager Interface
* Network Data Manager Interface
* Response Data Parser Interface



### Architecture Notes : 

* Each Coordinator has access to View Interfaces and communication is done via delegation 

* Coordinator has reference to View Interface and View Interface has delegate to Coordinator 

* Each Coordinator in every module conform to Coordinator Interface

* Each View Controller in every module conform to View Interface

* Persistent Store Data Manager Interface and Network Data Manager Interface inherits from  Data Manager Interface

* Data Manager Interface has the methods to model manipulation across all modules

* Network Data Manager Interface  has the methods for network calls and processing can be overridden by many concrete managers like Rest-API handling using Alamofire , GraphQL handling which make it flexible to change the network layer manager each time for specific module without changing anything 

* Persistent Store Data Manager Interface  has the methods for data persistence and data manipulation can be overridden by many concrete managers like Core-Data , Realm which make it flexible to change the persistence layer manager each time for specific module without changing anything 

* Response Data Parser Interface represent the interface for parsing response from  Network Data Manager Interface orPersistent Store Data Manager Interface to choose the parser on this level can be overridden by many concrete parser like XML-Parser / JSON Parser which make it flexible to change the  parsing layer manager each time for specific module without changing anything 

* Base Presenter is concrete class has implementation of common things used by most presenters on the app like Internet Connection Handling 

* Error Handling : Errors are handled  along with Internet Connection Error


### Librares/Frameworks 
I used the following Concrete libraries : 

* Persistent Store Data Manager Interface : Core-Data(Magical-Record)  / Realm 
* Network Data Manager Interface : Alamofire / Afnetworking 
* Response Data Parser Interface : JSON Parser(Mappable) / XML Parser (Not supported by back-end )
* Dependency Injection : Swinject
* Image Caching : SDWebImage
* Stubs / Unit-Testing : Mockingjay


### Code Samples 
Code Sample [1] :

* We used Network Manager uses Rest-API and Alamofire
* We used JSON Parser as Response Parser
* We used Core Data as Persistent Manager 


```swift
        /*
     Injecting:
     Network Manager: First Rest-API / Alamofire
     Database Manager: Core-Data
     Response Parser: First JSON Parser (Mappable)
     */
    func addFirstConfiguration(in container: Container) {
        
        container.register(SearchMoviesBuilder.self, factory: { SearchMoviesBuilder(resolver: $0) })
        
        container.register(MovieJSONDataParser.self, factory: { MovieJSONDataParser(resolver: $0) })
        
        container.register(MoviesNetworkDataManager.self, factory: { MoviesNetworkDataManager(resolver: $0, moviesResponseParserInterface:
        ($0.resolve(MovieJSONDataParser.self)) ?? MovieJSONDataParser(resolver: $0)) })
        
        container.register(MoviesQueriesCoreDataPersistentManager.self, factory: { MoviesQueriesCoreDataPersistentManager(resolver: $0) })
        
        container.register(SearchMoviesViewBuilder.self, factory:{ SearchMoviesViewBuilder(moviesNetworkManagerInterface: ($0.resolve(MoviesNetworkDataManager.self)) ?? 
        MoviesNetworkDataManager(resolver: $0, moviesResponseParserInterface: ($0.resolve(MovieJSONDataParser.self)) ?? MovieJSONDataParser(resolver: $0)), 
        moviePersistentStoreManagerInterface : ($0.resolve(MoviesQueriesCoreDataPersistentManager.self)) ?? MoviesQueriesCoreDataPersistentManager(resolver: $0))})
        
    }
```


Code Sample [2] :

* We used Network Manager uses Rest-API and Alamofire
* We used JSON Parser as Response Parser
* We used Realm as Persistent Manager 

```swift
    /*
     Injecting:
     Network Manager: First Rest-API / Alamofire
     Database Manager: Realm
     Response Parser: First JSON Parser (Mappable)
     */
    func addSecondConfiguration(in container: Container) {
        
        container.register(SearchMoviesBuilder.self, factory: { SearchMoviesBuilder(resolver: $0) })
        
        container.register(MovieJSONDataParser.self, factory: { MovieJSONDataParser(resolver: $0) })
        
        container.register(MoviesNetworkDataManager.self, factory: { MoviesNetworkDataManager(resolver: $0, moviesResponseParserInterface: 
        ($0.resolve(MovieJSONDataParser.self)) ?? MovieJSONDataParser(resolver: $0)) })
        
        container.register(MoviesQueriesRealmPersistentManager.self, factory: { MoviesQueriesRealmPersistentManager(resolver: $0) })
        
        container.register(SearchMoviesViewBuilder.self, factory:{ SearchMoviesViewBuilder(moviesNetworkManagerInterface: ($0.resolve(MoviesNetworkDataManager.self)) ??
        MoviesNetworkDataManager(resolver: $0, moviesResponseParserInterface: ($0.resolve(MovieJSONDataParser.self)) ?? MovieJSONDataParser(resolver: $0)),
        moviePersistentStoreManagerInterface : ($0.resolve(MoviesQueriesRealmPersistentManager.self)) ?? MoviesQueriesRealmPersistentManager(resolver: $0))})
        
    }
```

Code Sample [3]  :

* We used Network Manager uses Rest-API and AFNetworking 
* We used JSON Parser as Response Parser
* We used Core Data  as Persistent Manager 

```swift
    /*
     Injecting:
     Network Manager: Second Rest-API / AFNetworking
     Database Manager: Core-Data
     Response Parser: First JSON Parser (Mappable)
     */
    func addThirdConfiguration(in container: Container) {
        
        container.register(SearchMoviesBuilder.self, factory: { SearchMoviesBuilder(resolver: $0) })
        
        container.register(MovieJSONDataParser.self, factory: { MovieJSONDataParser(resolver: $0) })
        
        container.register(MovieNetworkSecondDataManager.self, factory: { MovieNetworkSecondDataManager(resolver: $0, moviesResponseParserInterface: 
        ($0.resolve(MovieJSONDataParser.self)) ?? MovieJSONDataParser(resolver: $0)) })
        
        container.register(MoviesQueriesCoreDataPersistentManager.self, factory: { MoviesQueriesCoreDataPersistentManager(resolver: $0) })
        
        container.register(SearchMoviesViewBuilder.self, factory:{ SearchMoviesViewBuilder(moviesNetworkManagerInterface: ($0.resolve(MovieNetworkSecondDataManager.self)) ??
        MovieNetworkSecondDataManager(resolver: $0, moviesResponseParserInterface: ($0.resolve(MovieJSONDataParser.self)) ?? MovieJSONDataParser(resolver: $0)),
        moviePersistentStoreManagerInterface : ($0.resolve(MoviesQueriesCoreDataPersistentManager.self)) ?? MoviesQueriesCoreDataPersistentManager(resolver: $0))})
        
    }
```

Code Sample [4] :

* We used Network Manager uses Rest-API and AFNetworking 
* We used XML Parser as Response Parser
* We used Core Data  as Persistent Manager 

```swift
    /*
     Injecting:
     Network Manager: Second Rest-API / AFnetworking
     Database Manager: Core-Data
     Response Parser: XML Parser
     */
    func addForthConfiguration(in container: Container) {
        
        container.register(SearchMoviesBuilder.self, factory: { SearchMoviesBuilder(resolver: $0) })
        
        container.register(MovieXMLDataParser.self, factory: { MovieXMLDataParser(resolver: $0) })
        
        container.register(MoviesNetworkDataManager.self, factory: { MoviesNetworkDataManager(resolver: $0, moviesResponseParserInterface: 
        ($0.resolve(MovieXMLDataParser.self)) ?? MovieXMLDataParser(resolver: $0)) })
        
        container.register(MoviesQueriesCoreDataPersistentManager.self, factory: { MoviesQueriesCoreDataPersistentManager(resolver: $0) })
        
        container.register(SearchMoviesViewBuilder.self, factory:{ SearchMoviesViewBuilder(moviesNetworkManagerInterface: ($0.resolve(MoviesNetworkDataManager.self)) ??
        MoviesNetworkDataManager(resolver: $0, moviesResponseParserInterface: ($0.resolve(MovieXMLDataParser.self)) ?? MovieXMLDataParser(resolver: $0)),
        moviePersistentStoreManagerInterface : ($0.resolve(MoviesQueriesCoreDataPersistentManager.self)) ?? MoviesQueriesCoreDataPersistentManager(resolver: $0))})
        
    }
```



* Every Network Manager, Persistent Manager , Response Parser can be replaced easily without no change (Liskov Substitution Principle)
* All 4 configuration are set on AppAssembly file for injection dependencies 


